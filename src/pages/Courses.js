import { Fragment, useEffect, useState } from "react";
// import coursesData from "../data/coursesData";
import CourseCard from "../components/CourseCard";

export default function Courses() {
    // Checks to see if the mock data was captured
    // console.log(coursesData);
    // console.log(coursesData[0]);

    // State that will be used to store the courses retieved form the database
    const [courses, setCourses] = useState([]);

    // Retrieves the courses from the database upon initial render of the "Courses" component
    useEffect(() => {
        fetch('http://localhost:4000/courses/all')
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // Sets the "courses" stte to map the dat retrieved from the fetch request into several 'CourseCard' component
            setCourses(data.map(course => {
                return (
                    <CourseCard key={course._id} courseProp={course} />
                );
            }))
        })
    }, []);

    return(
        <Fragment>
            {courses}
        </Fragment>
    )
}