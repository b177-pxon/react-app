import React from 'react';
import { Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function PageNotFound(){
    return(
        <Row>
            <h1>Page Not Found</h1>
            <p>Go back to the <Link as={Link} to='/' exact>homepage.</Link></p>
        </Row>
    )
}