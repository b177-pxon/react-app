const coursesData = [
    {
        id : "wdc001",
        name : "PHP - Laravel",
        description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque dolor magnam vero sapiente quidem, distinctio iste, dolore exercitationem consectetur natus esse et hic, quis accusamus? Omnis quibusdam alias delectus libero?",
        price : 45000,
        onOffer : true
    },
    {
        id : "wdc002",
        name : "Python - Django",
        description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque dolor magnam vero sapiente quidem, distinctio iste, dolore exercitationem consectetur natus esse et hic, quis accusamus? Omnis quibusdam alias delectus libero?",
        price : 50000,
        onOffer : true
    },
    {
        id : "wdc003",
        name : "Java - Springboot",
        description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque dolor magnam vero sapiente quidem, distinctio iste, dolore exercitationem consectetur natus esse et hic, quis accusamus? Omnis quibusdam alias delectus libero?",
        price : 55000,
        onOffer : true
    }
];
export default coursesData;